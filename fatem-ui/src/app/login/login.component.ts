import { Component, OnInit } from '@angular/core';
import {LoginService} from '../../app/shared/services/login.service';
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})

export class LoginComponent  {

  form = {
    user: '',
    password: ''
    };

    constructor(public log: LoginService
      ) {
        this.log.err = null;
      }
login() {
this.log.login(this.form);
}
}
