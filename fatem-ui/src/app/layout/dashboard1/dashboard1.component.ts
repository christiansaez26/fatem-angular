import { Component, OnInit } from '@angular/core';
import { HistorialService } from 'src/app/shared/services/historial.service';

export interface PeriodicElement {
idAlarma: string;
agencia: string;
planta: string;
extLocal: string;
cable: number;
armario: string;
parInicial: string;
parFinal: string;
detalleCfn: string;
creacion: string;
ticket: number;
descripcionTicket: string;
tecno: string;
motivoFalla: string;
afectados: number;
atiende: string;
  }
@Component( {
    selector: 'app-dashboard1',
    templateUrl: './dashboard1.component.html',
    styleUrls: ['./dashboard1.component.scss'],
    providers: [HistorialService]
    
})
export class Dashboard1Component implements OnInit {


    ngOnInit() {
}
// displayedColumns : string[] = this.historial.column;
// ['elemento', 'planta', 'hostname','resumen', 'zona', 'cantidad'];
constructor(public historial: HistorialService) {
this.historial.dataTable = null;
}
}