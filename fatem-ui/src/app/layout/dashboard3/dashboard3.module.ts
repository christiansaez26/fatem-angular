import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule, MatCardModule, MatIconModule, MatTableModule,MatNativeDateModule } from '@angular/material';
import { MatGridListModule } from '@angular/material/grid-list';

import { StatModule } from '../../shared/modules/stat/stat.module';
import { Dashboard3RoutingModule } from './dashboard3-routing.module';
import { Dashboard3Component } from './dashboard3.component';
import {MatPaginatorModule} from '@angular/material/paginator';
import { FormsModule } from '@angular/forms';
import {MatFormFieldModule} from '@angular/material/form-field';
import {Form3Component} from '../components/form3/form3.component';
import {MatSelectModule} from '@angular/material/select';
import {MatInputModule} from '@angular/material';
import { NgxSpinnerModule } from 'ngx-spinner';
import {MatDatepickerModule} from '@angular/material/datepicker';





@NgModule( {
    imports: [
        CommonModule,
        MatNativeDateModule,
        MatDatepickerModule,
        Dashboard3RoutingModule,
        MatGridListModule,
        StatModule,
        MatCardModule,
        MatCardModule,
        MatTableModule,
        MatButtonModule,
        MatIconModule,
        MatPaginatorModule,
        FormsModule,
        MatFormFieldModule,
        MatSelectModule,
        MatInputModule,
        NgxSpinnerModule,
        FlexLayoutModule.withConfig({addFlexToParent: false})
    ],
    declarations: [Dashboard3Component, Form3Component]})
export class Dashboard3Module {}
