import { Component, OnInit } from '@angular/core';
import { TicketsService } from 'src/app/shared/services/tickets.service';

@Component({
  selector: 'app-dashboard3',
  templateUrl: './dashboard3.component.html',
  styleUrls: ['./dashboard3.component.scss']
})
export class Dashboard3Component implements OnInit {


  displayedColumns: string[] = ['idTicket', 'fechaCreacion', 'sistema', 'cantidad'];

  constructor(public ticket: TicketsService) {

    this.ticket.dataTable = null;
  }

    ngOnInit() {}

}
export interface PeriodicElement {
  idTicket:  number;
  fechaCreacion: string;
  sistema: string;
  cantidad: number;
}
