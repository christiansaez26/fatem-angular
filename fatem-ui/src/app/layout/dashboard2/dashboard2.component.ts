import {Component, OnInit} from '@angular/core';
import { AlarmasService } from '../../shared/services/alarmas.service';
/**
 * @title Table with pagination
 */
@Component({
    selector: 'app-dashboard2',
  templateUrl: './dashboard2.component.html',
  styleUrls: ['./dashboard2.component.scss'],
  providers: [AlarmasService]

})
export class Dashboard2Component implements OnInit {

  displayedColumns: string[] = ['id', 'fechaevento', 'gravedad', 'zona', 'elemento', 'diagnostico', 'planta', 'nombre', 'totalclientes'];

  ngOnInit() {
  }
  constructor(public alarmas: AlarmasService) {
  this.alarmas.dataTable = null;
}
}

export interface PeriodicElement {
id: number;
fechaevento: string;
gravedad: string;
zona: string;
elemento: string;
diagnostico: string;
planta: string;
nombre: string;
totalclientes: number;
}
