import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialogRef, MatDialog } from '@angular/material';
import { Subscription } from 'rxjs';
import { ActivityMonitorService } from 'src/app/shared/services/activity-monitor.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-message',
  templateUrl: './modal-message.component.html',
  styleUrls: ['./modal-message.component.scss']
})
export class MessageComponent {

  status: any;
  getAMStatusSubscription: Subscription;

  constructor(
    public dialogRef: MatDialogRef<MessageComponent>,
    public dialog: MatDialog,
    public amService: ActivityMonitorService,
    public router: Router
  ) { }

  onClose() {
    this.dialog.closeAll();
  }
}
