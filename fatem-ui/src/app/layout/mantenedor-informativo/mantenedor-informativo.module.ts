import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule, MatCardModule, MatIconModule, MatTableModule,MatNativeDateModule } from '@angular/material';
import { MatGridListModule } from '@angular/material/grid-list';
import { StatModule } from '../../shared/modules/stat/stat.module';
import { MantenedorInformativoRoutingModule } from './mantenedor-informativo-routing.module';
import { MantenedorInformativoComponent } from './mantenedor-informativo.component';
import {MatPaginatorModule} from '@angular/material/paginator';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatFormFieldModule} from '@angular/material/form-field';
import {
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatSidenavModule,
    MatToolbarModule
} from '@angular/material';
import { TranslateModule } from '@ngx-translate/core';
import { MatSelectModule} from '@angular/material';
import {MatRadioModule} from '@angular/material/radio';
import { NgxSpinnerModule } from 'ngx-spinner';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { 
    IgxTimePickerModule,
    IgxInputGroupModule,
	IgxIconModule
 } from "igniteui-angular";
 import { 
    OwlDateTimeModule, 
    OwlNativeDateTimeModule 
  } from 'ng-pick-datetime';
 


@NgModule({
    imports: [
        CommonModule,
        IgxTimePickerModule,
        OwlDateTimeModule,
        OwlNativeDateTimeModule,
        IgxInputGroupModule,
        IgxIconModule,
        MantenedorInformativoRoutingModule,
        MatNativeDateModule,
        MatDatepickerModule,
        MatGridListModule,
        StatModule,
        MatCardModule,
        MatTableModule,
        MatPaginatorModule,
        FormsModule,
        MatToolbarModule,
        MatButtonModule,
        MatSidenavModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatListModule,
        TranslateModule,
        MatFormFieldModule,
        MatSelectModule,
        MatRadioModule,
        NgxSpinnerModule,
        ReactiveFormsModule,
        FlexLayoutModule.withConfig({addFlexToParent: false})
    ],
    declarations: [MantenedorInformativoComponent]}
    )
export class MantenedorInformativoModule {}
