import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MantenedorInformativoComponent } from './mantenedor-informativo.component';

const routes: Routes = [
    {
        path: '',
        component: MantenedorInformativoComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class MantenedorInformativoRoutingModule {}
