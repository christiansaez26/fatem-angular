import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MantenedorInformativoComponent } from './mantenedor-informativo.component';

describe('MantenedorInformativoComponent', () => {
  let component: MantenedorInformativoComponent;
  let fixture: ComponentFixture<MantenedorInformativoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MantenedorInformativoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MantenedorInformativoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
