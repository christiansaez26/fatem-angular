import { Component, OnInit } from '@angular/core';
import { ArchivoService } from 'src/app/shared/services/archivo.service';
import { ServiceSelectService } from 'src/app/shared/services/service-select.service';
import { FormControl, NgModel, Validators } from '@angular/forms';
import { ZonaService } from 'src/app/shared/services/zona.service';
import { AgenciaService } from 'src/app/shared/services/agencia.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { MessageComponent } from '../modal-message/modal-message.component';
import { MatDialog } from '@angular/material';
import { Idle } from '@ng-idle/core';
import { ActivityMonitorService } from 'src/app/shared/services/activity-monitor.service';
import * as moment from 'moment';




let formData = new FormData();

export interface Estructura {
  id:string,
  name: string
}

@Component({
  selector: 'app-mantenedor-informativo',
  templateUrl: './mantenedor-informativo.component.html',
  styleUrls: ['./mantenedor-informativo.component.scss']
})



export class MantenedorInformativoComponent implements OnInit {
  listServicio: any;
  listTiposAlarma: any;
  verSeleccion: any;
  opcionAlarma;
  idTipoAlarma: any;
  opcionServicios: any;
  verSeleccionServ: any;
  idServicio: any;
  horaInicio;
  horaFin;
  exito=false;
  alert=0;
  addfileA=false;

  servicio={
    id:'',
    nombre:''
  }

  form={
    //file: new FormData(),
    obs:'',
    idTipoAlarma:'',
    servicios: this.servicio,
    fechaInicio:'',
    horaInicio:'',
    fechaFin:'',
    horaFin:'',
    zona:'',
    agencia:''
  };
  form1={
    //file: new FormData(),
    obs:'',
    idTipoAlarma:'',
    servicios: this.servicio,
    fechaInicio:'',
    horaInicio:'',
    fechaFin:'',
    horaFin:'',
    zona:'',
    agencia:''
  };

  error: any;
  servicios: any;
  selectedServicio: any;
  files: any;
  zonas: any;
  opcionAgencia: string;
  opcionZona: any;
  idZona: any;
  agencias: any;
  verSeleccionA: string;
  idAgencia: string;
  timedOut: boolean;
  idleState: string;
  status: { state: string; active: boolean; };
  valid1: boolean;
  valid2: boolean;
  valid3: boolean;
  valid4: boolean;
  valid5: boolean;
  valid6: boolean;
  valid7: boolean;
  valid8: boolean;
  fecha;
  fechaFin: any;

  
  constructor(public Archivo:ArchivoService, public Select:ServiceSelectService, 
              public zona:ZonaService, public agencia:AgenciaService,public spinner: NgxSpinnerService,
              public dialog: MatDialog,
              private idle: Idle,
              private amService: ActivityMonitorService) { 

                this.zona.getZona().subscribe(rsp=>{
                  this.zonas = rsp['listGeo'];
            
                },err=>{
                  this.error = err.status; // captura error
                });
                this.onSelect();
    

                this.idZona='';
                this.idAgencia='';

  }

  fileSelect = new FormControl('', [Validators.required]);
  zonaSelect = new FormControl('', [Validators.required]);
  agenciaSelect = new FormControl('', [Validators.required]);
  servicioSelect = new FormControl('', [Validators.required]);
  alarmaSelect = new FormControl('', [Validators.required]);
  observacion = new FormControl('', [Validators.required]);
  fecIni = new FormControl('', [Validators.required]);
  fecFin = new FormControl('', [Validators.required]);
  valid;
  

  fileData(){
    if(this.files==null){
      this.valid=true;

    }else{
      this.valid=false;
    }
  }

  public start(): void {
    this.idle.watch();
    this.idleState = 'Started.';
    }
    public stop(): void {
    this.idle.stop();
    this.dialog.closeAll();
    }
  CapturaZona() {
    this.opcionAgencia= '';
    this.idZona='';
    this.verSeleccion = this.opcionZona;
    this.idZona= this.verSeleccion;
    this.idAgencia='';
    this.Agencia();
  }
Agencia(){
  //agencia
  this.spinner.show();
  this.agencia.getAgencia(this.idZona).subscribe(rsp=>{
    this.agencias = rsp['listGeo'];
    this.spinner.hide();

  },err=>{
    this.error = err.status; // captura error
    this.error=null;
    this.agencias=null;
    this.spinner.hide();

  });
}


CapturaAgencia() {

  this.idAgencia='';
  this.verSeleccionA = this.opcionAgencia;
  this.idAgencia= this.verSeleccionA;
}

  ngOnInit() {
  }

  onSelect(){
    
    this.Select.getSelect().subscribe(rsp=>{
      this.listServicio = rsp['servicios'];
      this.listTiposAlarma = rsp['tiposAlarma'];
  
    },err=>{
      this.error = err.status; // captura error
    });
  }

   CapturaAlarma() {     
    this.verSeleccion = this.opcionAlarma;
    this.idTipoAlarma= this.verSeleccion;
   }

   CapturaServicio() {     
    this.verSeleccionServ = this.opcionServicios;
    this.idServicio= this.verSeleccionServ;
    
   }

   seleccionado;
   select() {
    this.selectedServicio;
    this.servicio=this.selectedServicio;

   }

   
   selectAll(checkAll, cselect: NgModel, values) {
    if(checkAll){
      
      cselect.update.emit(values); 
    }
    else{
      cselect.update.emit([]);
    }
  }

limpiar(){

  this.form.obs='';
  this.selectedServicio='';
  this.opcionAlarma='';
  this.form.horaFin='';
  this.form.horaInicio='';
  this.form.idTipoAlarma='';
  this.form.fechaFin='';
  this.form.fechaInicio='';
  this.form.servicios=null;
  this.files=null;
  this.opcionAgencia='';
  this.opcionZona='';
  this.error=null;
  this.valid=null;
  this.valid1=false;
  this.valid2=false;
  this.valid3=false;
  this.valid4=false;
  this.valid5=false;
  this.valid6=false;
  this.valid7=false;
  this.valid8=false;
  if(this.opcionZona==''){
    this.agencias=null;
  }
  this.fileSelect = new FormControl();
  this.zonaSelect = new FormControl();
  this.agenciaSelect = new FormControl();
  this.servicioSelect = new FormControl();
  this.alarmaSelect = new FormControl();
  this.observacion = new FormControl();
  this.fecIni = new FormControl();
  this.fecFin = new FormControl();
}

  Guardar(){
    
    this.form.obs;
    this.form.idTipoAlarma = this.idTipoAlarma;

    this.servicio = this.selectedServicio;//this.servicios.value[];
    this.form.servicios= this.servicio; //this.idServicio;


    if(this.form.horaInicio =='' || this.form.horaInicio==null){
      this.form.horaInicio = '00:00';
    }
    if(this.form.horaFin ==''|| this.form.horaFin==null){
      this.form.horaFin = '00:00';
    }

    if(!this.fileSelect.valid){
      this.valid1=true;
    }else{
      this.valid1=false;

    }
    if(!this.zonaSelect.valid){
      this.valid2=true;
    }else{
      this.valid2=false;

    }
    if(!this.agenciaSelect.valid){
      this.valid3=true;
    }else{
      this.valid3=false;

    }
    if(this.servicioSelect.value==null || this.servicioSelect.value=='' ){
      this.valid6=true;
    }else{
      this.valid6=false;

    }
    if(!this.alarmaSelect.valid){
      this.valid5=true;
    }else{
      this.valid5=false;

    }
    if(!this.observacion.valid){
      this.valid4=true;
    }else{
      this.valid4=false;

    }
    if(this.form.fechaInicio==null || this.form.fechaInicio==''){
      this.valid7=true;
    }else{
      this.valid7=false;

    }
    if(this.form.fechaFin==null||this.form.fechaFin==''){
      this.valid8=true;
    }else{
      this.valid8=false;

    }

    this.form1.horaInicio=this.convertTimeToString(this.form.horaInicio)
    this.form1.horaFin=this.convertTimeToString(this.form.horaFin)

    this.form.zona=this.idZona;
    this.form.agencia=this.idAgencia;
//carga form1
    this.form1.fechaInicio= this.convertDateToString(this.form.fechaInicio);
    this.form1.fechaFin= this.convertDateToString(this.form.fechaFin);
    this.form1.agencia=this.form.agencia;
    this.form1.zona=this.form.zona;
    this.form1.servicios=this.form.servicios; //this.idServicio;
    this.form1.idTipoAlarma=this.form.idTipoAlarma;
    this.form1.obs=this.form.obs;

    this.spinner.show();

    this.Archivo.GuardaRegistro(this.form1).subscribe(rsp=>{
      //this.agencias = rsp['listGeo'];
      const idAlarma = rsp.id;
      this.exito=true;
      this.alert=200;
      
      this.start();
        this.idleState = 'Se ha creado Alarma ID: '+idAlarma;
        this.timedOut = true;
        this.status = { state: this.idleState, active: false };
        this.amService.setStatus(this.status);
        this.openDialog();

      ///borra datos siendo un exito
        this.form.obs='';
        this.selectedServicio='';
        this.servicioSelect=null;
        this.opcionAlarma='';
        this.form.horaFin='';
        this.form.horaInicio='';
        this.form.idTipoAlarma='';
        this.form.fechaFin='';
        this.form.fechaInicio='';
        this.form.servicios=null;
        this.files=null;
        this.opcionAgencia='';
        this.opcionZona='';
        this.error=null;
        this.valid1=null;
        this.valid2=null;
        this.valid3=null;
        this.valid4=null;
        this.valid5=null;
        this.valid6=null;
        this.valid7=null;
        this.valid8=null;

        if(this.opcionZona==''){
          this.agencias=null;
        }

        this.fileSelect = new FormControl();
        this.zonaSelect = new FormControl();
        this.agenciaSelect = new FormControl();
        this.servicioSelect = new FormControl();
        this.alarmaSelect = new FormControl();
        this.observacion = new FormControl();
        this.fecIni = new FormControl();
        this.fecFin = new FormControl();
        //
        this.form1.obs='';
        this.form1.horaFin='';
        this.form1.horaInicio='';
        this.form1.idTipoAlarma='';
        this.form1.fechaFin='';
        this.form1.fechaInicio='';
        this.form1.servicios=null;

        ////
      this.spinner.hide();

    },err=>{
      this.error = err.status; // captura error
      this.exito=false;
      this.alert=0;
      if(this.form.obs===''||this.form.idTipoAlarma===''||this.form.servicios===null||
      this.form.fechaInicio===''||this.form.fechaFin===''||this.form.zona===''||
      this.form.agencia===''||(this.fileSelect===null||this.fileSelect.invalid)){
        this.error=420;
      }

      
      this.spinner.hide();

    });
  }
  
  convertDateToString(dateToBeConverted: string) {

    if (dateToBeConverted===''){
      value='';
     }else{
      var value = moment(dateToBeConverted).format("YYYY-MM-DD");
     }
     return value;
     }

     convertTimeToString(dateToBeConverted: Object) {

      if (dateToBeConverted==='00:00'){
        value='00:00';
       }else{
        var value = moment(dateToBeConverted).format("HH:mm");
       }
       return value;
       }
  openDialog(): void {
    const dialogRef = this.dialog.open(MessageComponent, {
    });
    dialogRef.afterClosed().subscribe(result => {
      this.stop();
      setTimeout(() => {
        /** spinner ends after 5 seconds */
        this.alert=null;
        this.exito=null;

      }, 10000);
    });
    }

}