import {Component} from '@angular/core';
import {AlarmasService} from '../../../shared/services/alarmas.service';
import { ZonaService } from 'src/app/shared/services/zona.service';
import { AgenciaService } from 'src/app/shared/services/agencia.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'app-form2',
    templateUrl: './form2.component.html',
    styleUrls: ['./form2.component.scss']
})

export class Form2Component {


  form = {
    idAgencia:'',
    planta: '',
    elemento: ''
    };
  
  
  disable: boolean=false;
  zonas: any;
  opcionAgencia: string;
  verSeleccion: any;
  opcionZona: any;
  idZona: any;
  agencias: any;
  verSeleccionA: string;
  idAgencia: string;
  valida: boolean=false;

    limpiar() {
      this.form.planta = '';
      this.form.elemento = '';
      this.form.idAgencia=null;   
      this.opcionAgencia= '';         
      this.opcionZona= '';     
      this.idZona=''; 
      this.alarmas.err = null;
      this.alarmas.limpiaTabla();
      this.alarmas.disable=false;
      this.idAgencia='';
      this.verSeleccion='';
      this.verSeleccionA='';
      this.valida=false;

      
  if(this.idZona==''){
    this.agencias=null;
  }

        }
    consultar() {
      this.alarmas.dataTable = null;
      this.form.idAgencia = this.idAgencia;

      if(this.opcionZona!=''){
        if(this.opcionAgencia!=''){
          this.valida=false;
          this.alarmas.consultar(this.form, this.valida);
        }else{
          this.valida=true;
          this.alarmas.err=null;
        }
      }else{
        this.valida=false;
        this.alarmas.consultar(this.form, this.valida);
      }

  }
  constructor(
    public alarmas: AlarmasService, public zona: ZonaService, public agencia:AgenciaService, private spinner: NgxSpinnerService
    ) { this.zona.getZonaAlarm().subscribe(rsp=>{
      this.zonas = rsp['listGeo'];

    },err=>{
      
    });



  }

  CapturaZona() {
    this.opcionAgencia= '';   
    this.idAgencia=''; 
    this.idZona='';
    this.verSeleccion = this.opcionZona;
    this.idZona= this.verSeleccion;
    this.Agencia(this.idZona);
  }
Agencia(idZona){
  //agencia
  this.spinner.show();
  this.agencia.getAgenciaAlarm(idZona).subscribe(rsp=>{
    this.agencias = rsp['listGeo'];
    this.spinner.hide();

  },err=>{
    this.agencias=null;
    this.spinner.hide();


    
  });
}

CapturaAgencia() {
  this.idAgencia='';
  this.verSeleccionA = this.opcionAgencia;
  this.idAgencia= this.verSeleccionA;
}
}
