import { Component, ViewChild } from '@angular/core';
import {HistorialService} from '../../../shared/services/historial.service';
import { MatRadioButton} from '@angular/material';
import { FormControl, Validators } from '@angular/forms';
import * as moment from 'moment';



@Component({
    selector: 'app-form1',
    templateUrl: './form1.component.html',
    styleUrls: ['./form1.component.scss'],
    
})

export class Form1Component {

  form = {
    fechaInicio: '',
    fechaFin: '',
    planta: ''
  };

  form1 = {
    fechaInicio: '',
    fechaFin: '',
    planta: ''
  };

  @ViewChild('externa')
  RB: MatRadioButton;
  displayedColumns: string[];
  planta;
  obligatorio='';
  disable: boolean=false;
  fecValid: boolean=false;
  filtro2: string;

  fecInicio = new FormControl('', [Validators.required]);
  fecFin = new FormControl('', [Validators.required]);
  
  onSelectionChange() {
    this.historial.dataTable = null;
    if (this.RB.checked === true) {// valida si es externa o no y imprime cabecera correspondiente
      this.displayedColumns = ['idAlarma','creacion', 'ticket', 'agencia', 'descripcionTicket', 'tecno', 'motivoFalla', 'afectados', 'atiende'];
    } else {
      this.displayedColumns = ['idAlarma','cfn', 'fecha', 'agencia', 'planta', 'extLocal', 'cable', 'armario', 'parInicial', 'parFinal', 'detalleCfn'];
    }
    this.historial.cabecera(this.displayedColumns);
  }

    limpiar() {
      this.form.fechaInicio = '';
      this.form.fechaFin = '';
      this.form.planta = '';
      this.historial.err = null;
      this.historial.displayedColumns = null;
      this.historial.limpiaTabla();
      this.historial.disable=false;
      this.form.fechaInicio = null;
      this.form.fechaFin = null;
      this.fecInicio = new FormControl();
      this.fecFin = new FormControl();
      this.form.fechaInicio = '.';
      this.form.fechaFin = '.';
      this.form.fechaInicio = '';
      this.form.fechaFin = '';
    }
    consultar() {
      this.historial.dataTable = null;
      this.historial.displayedColumns = this.displayedColumns;
      this.obligatorio='';
      this.fecValid==true;
      if(this.form.fechaInicio === '' || this.form.fechaInicio ===null){
        this.obligatorio= 'Fecha Inicio';
      };
      if(this.form.fechaFin === '' || this.form.fechaFin ===null) {
        if(this.obligatorio!=''){
          this.obligatorio= this.obligatorio+',' + ' Fecha Fin';
        }else{
          this.obligatorio= this.obligatorio + ' Fecha Fin';

        }
      };
      if(this.form.planta === ''){
        if(this.obligatorio!=''){
          this.obligatorio= this.obligatorio+',' + ' Planta';
        }else{
          this.obligatorio= this.obligatorio + ' Planta';
        }
      };



      
          this.form1.fechaInicio=this.convertDateToString(this.form.fechaInicio);
          this.form1.fechaFin=this.convertDateToString(this.form.fechaFin);
          this.form1.planta=this.form.planta;

      this.historial.consultar(this.form1, this.obligatorio);
  
      

  }


  convertDateToString(dateToBeConverted: string) {
   
   if (dateToBeConverted===''){
    value='';
   }else{
    var value = moment(dateToBeConverted).format("YYYY-MM-DD");
   }
    return value;
    }
    
    
  constructor(
    public historial: HistorialService
    ) {
      this.form.planta = '15';
      this.displayedColumns = ['idAlarma', 'creacion', 'ticket', 'agencia', 'descripcionTicket', 'tecno', 'motivoFalla', 'afectados', 'atiende'];
      this.historial.cabecera(this.displayedColumns);
  }
}