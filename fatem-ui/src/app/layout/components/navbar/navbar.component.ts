import { Component, Inject, ViewChild } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { LoginService} from '../../../shared/services/login.service';
import { MatDialog} from '@angular/material';
import { ActivityMonitorService } from 'src/app/shared/services/activity-monitor.service';
import { MessageComponent } from '../../modal-message/modal-message.component';

export interface DialogData {
    animal: string;
    name: string;
  }
@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent  {
    public pushRightClass: string;
    log: Boolean;
    usuario = this.login.usuario;
  idleState: string;
  status: { state: string; active: boolean; };
    constructor(public dialog: MatDialog,
      public router: Router ,
      private translate: TranslateService,
      public login: LoginService,
      public amService: ActivityMonitorService

        ) {
        this.router.events.subscribe(val => {

            if (val instanceof NavigationEnd && window.innerWidth <= 992 && this.isToggled()) {
                this.toggleSidebar();
            }
        });
    }


    openDialog(): void {

      if (this.login.getPerfil() !== 'Admin' ) {
        this.idleState = 'Error!, Acceso Denegado';
        this.status = { state: this.idleState, active: false };
        this.amService.setStatus(this.status);
        const dialogRef = this.dialog.open(MessageComponent, {
          width: '50%',
        });
        dialogRef.afterClosed().subscribe(result => {
          this.router.navigate(['/dashboard1']);
        });
      }
    }
    isToggled(): boolean {
        const dom: Element = document.querySelector('body');
        return dom.classList.contains(this.pushRightClass);
    }

    toggleSidebar() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle(this.pushRightClass);
    }
    Loggedout() {
        localStorage.setItem('login', 'true');
        localStorage.removeItem('isLoggedin');
        this.router.navigate( ['/login'] );
    }
    onLoggedout() {
        this.Loggedout();
    }
    changeLang(language: string) {
        this.translate.use(language);
    }
}
