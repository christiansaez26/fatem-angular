import { Component, OnInit } from '@angular/core';
import { TicketsService } from 'src/app/shared/services/tickets.service';
import * as moment from 'moment';

@Component({
  selector: 'app-form3',
  templateUrl: './form3.component.html',
  styleUrls: ['./form3.component.scss']
})
export class Form3Component  {

  form = {
    fechaCreacion: ''
    };
    form1 = {
      fechaCreacion: ''
      };
  disable: boolean=false;


    limpiar() {
      this.ticket.err = null;
      this.ticket.limpiaTabla();
      this.ticket.disable=false;
      this.form1.fechaCreacion = null;
      this.form.fechaCreacion = null;

      if(this.form.fechaCreacion==null){
      }



    }
    consultar() {
      this.ticket.dataTable = null;
      this.form1.fechaCreacion=this.convertDateToString(this.form.fechaCreacion);
      this.ticket.consultar(this.form1);
      
  }
  
  convertDateToString(dateToBeConverted: string) {
    if (dateToBeConverted=='' || dateToBeConverted==null){
      value='';
     }else{
      var value = moment(dateToBeConverted).format("YYYY-MM-DD");
     }
     return value;    
    }

  constructor(
    public ticket: TicketsService,
    ) { 
      this.ticket.limpiaTabla();
      this.ticket.disable=false;
      this.ticket.err = null;


    }
}
