import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LayoutComponent } from './layout.component';

const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            {
                path: '',
                redirectTo: 'bienvenida'
            },
            {
                path: 'bienvenida',
                loadChildren: './bienvenida/bienvenida.module#BienvenidaModule'
            },
            {
                path: 'dashboard1',
                loadChildren: './dashboard1/dashboard1.module#Dashboard1Module'
            },
            {
                path: 'dashboard2',
                loadChildren: './dashboard2/dashboard2.module#Dashboard2Module'
            },
            {
                path: 'dashboard3',
                loadChildren: './dashboard3/dashboard3.module#Dashboard3Module'
            },
            {
                path: 'mantenedor-informativo',
                loadChildren: './mantenedor-informativo/mantenedor-informativo.module#MantenedorInformativoModule'
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class LayoutRoutingModule {}
