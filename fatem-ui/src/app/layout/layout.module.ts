import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import {
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatSidenavModule,
    MatToolbarModule,
    MatPaginatorIntl,
    MAT_DATE_LOCALE
} from '@angular/material';
import { TranslateModule } from '@ngx-translate/core';
import { NavbarComponent } from './components/navbar/navbar.component';
import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatFormFieldModule} from '@angular/material/form-field';
import { MatSelectModule} from '@angular/material';
import { NgxSpinnerModule } from 'ngx-spinner';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatDialogModule} from '@angular/material';
import { DialogModule } from '@syncfusion/ej2-angular-popups';
import { NgIdleKeepaliveModule } from '@ng-idle/keepalive';
import { MessageComponent } from './modal-message/modal-message.component';
import { getDutchPaginatorIntl } from '../shared/services/historial.service';



@NgModule({
    
    imports: [
        CommonModule,
        LayoutRoutingModule,
        MatToolbarModule,
        MatButtonModule,
        MatSidenavModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatListModule,
        TranslateModule,
        FormsModule    ,
        MatFormFieldModule,
        MatSelectModule,
        NgxSpinnerModule,
        MatExpansionModule,
        ReactiveFormsModule,
        DialogModule,
        MatDialogModule,
        NgIdleKeepaliveModule.forRoot()

        ],
    declarations: [LayoutComponent, NavbarComponent,MessageComponent],
    entryComponents: [
        MessageComponent
      ],
      providers: [
        { provide: MatPaginatorIntl, useValue: getDutchPaginatorIntl() }
      ,
        // The locale would typically be provided on the root module of your application. We do it at
        // the component level here, due to limitations of our example generation script.
        {provide: MAT_DATE_LOCALE, useValue: 'jp-JP'}
  
      ]
    })
export class LayoutModule { }
