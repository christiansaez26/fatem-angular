import { Component } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';
import { Router } from '@angular/router';
import {Idle, DEFAULT_INTERRUPTSOURCES} from '@ng-idle/core';
import { Keepalive } from '@ng-idle/keepalive';
import { MessageComponent } from './modal-message/modal-message.component';
import { ActivityMonitorService } from '../shared/services/activity-monitor.service';

@Component({
    selector: 'app-layout',
    templateUrl: './layout.component.html',
    styleUrls: ['./layout.component.scss']
})

export class LayoutComponent {

  status;
  idleState = 'Not started.';
  timedOut = false;
  hasIdled = false;
  lastPing?: Date = null;
  tokenExpires: any;



  constructor(private keepalive: Keepalive,
    private amService: ActivityMonitorService,
    private idle: Idle,
    public router: Router,
    public dialog: MatDialog) { // initiate it in your component constructor
      idle.setIdle(599); // Executes when inactive for 1 hour
      idle.setTimeout(1); // Executes a 15 minute countdown prior to Timeout
      idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);
      this.start();

      idle.onIdleEnd.subscribe(() => {
        this.hasIdled = false;
        this.idleState = `No longer idle.`;
        this.status = { state: this.idleState, active: this.hasIdled };
      });
      idle.onTimeout.subscribe(() => {
        this.idleState = 'Timed out!, Session Expirada';
        this.timedOut = true;
        this.status = { state: this.idleState, active: false };
        this.amService.setStatus(this.status);
        this.openDialog();
      });
      keepalive.interval(30); // Executes every 30 minutes
      keepalive.onPing.subscribe(() => {
        this.lastPing = new Date();
      });
      // this.start();
}
public start(): void {
this.idle.watch();
this.idleState = 'Started.';
this.timedOut = false;
}
public stop(): void {
this.idle.stop();
this.dialog.closeAll();
this.router.navigate(['/login']);
}
openDialog(): void {
localStorage.setItem('login', 'true');
localStorage.removeItem('isLoggedin');
const dialogRef = this.dialog.open(MessageComponent, {
});
dialogRef.afterClosed().subscribe(result => {
  this.stop();
});
}
}
