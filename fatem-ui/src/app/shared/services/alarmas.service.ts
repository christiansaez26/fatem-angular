
import { Injectable, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatPaginatorIntl } from '@angular/material';
import {environment} from '../../../environments/environment';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { PeriodicElement } from 'src/app/layout/dashboard2/dashboard2.component';
import { NgxSpinnerService } from 'ngx-spinner';

let dat;
let dataTabla: MatTableDataSource<PeriodicElement>;
const APIAlarmsEndpoint = environment.APIGetAlarmsEndpoint;
@Injectable({
  providedIn: 'root'
})
export class AlarmasService {
  err;
  dataTable;
  url = APIAlarmsEndpoint;

public pageSize = 10;
public currentPage = 0;
public totalSize = 0;
@ViewChild(MatPaginator) paginator: MatPaginator;
@ViewChild(MatSort) sort: MatSort;
disable: boolean=false;

  public handlePage(e: any) {
    this.currentPage = e.pageIndex;
    this.pageSize = e.pageSize;
    this.iterator();
  }
constructor(private http: HttpClient, private spinner: NgxSpinnerService) { }


limpiaTabla(){
  this.currentPage = null;
  this.totalSize = null;
  this.dataTable = null;
}
consultar(form, valida) {
this.err = null;
dat = null;
this.dataTable = null;
// TODO
// consulta http y mapeo de la data
const httpOptions = {
  headers: new HttpHeaders({
  'AUTH_API_KEY': 'fatem2019',
  })
  };
  this.spinner.show();

  this.http.post(this.url, form, httpOptions).subscribe(
data => {
dat = data['listDetalleAlarma'];
dataTabla = new MatTableDataSource<PeriodicElement>(dat);
dataTabla.paginator = this.paginator;
this.totalSize = dataTabla.data.length;
this.disable=true;
this.iterator();
  this.spinner.hide();
  },
  (error: Response) => {
      this.err = error.status;
      this.currentPage = null;
      this.totalSize = null;
      this.disable=false;
      valida=false;
      this.spinner.hide();
  }
  );
  }
  private iterator() {
    const end = (this.currentPage + 1) * this.pageSize;
    const start = this.currentPage * this.pageSize;
    const part = dataTabla.data.slice(start, end);
    this.dataTable = part;
    return `${start + 1} - ${end} de ${this.totalSize}`;

  }
}
const dutchRangeLabel = (page = this.currentPage, pageSize = this.pageSize, length = this.totalSize) => {
if (length === 0 || pageSize === 0) { return `0 de ${length}`; }
length = Math.max(length, 0);
const startIndex = page * pageSize;
// If the start index exceeds the list length, do not try and fix the end index to the end.
const endIndex = startIndex < length ?
Math.min(startIndex + pageSize, length) : startIndex + pageSize;

return `${startIndex + 1} - ${endIndex} de ${length}`;
};
export function  getDutchPaginatorIntl() {
const paginatorIntl = new MatPaginatorIntl();
paginatorIntl.itemsPerPageLabel = 'Elemento por pagina:';
// paginatorIntl.nextPageLabel = 'Volgende pagina';
// paginatorIntl.previousPageLabel = 'Vorige pagina';
paginatorIntl.getRangeLabel = dutchRangeLabel;
return paginatorIntl;
}
