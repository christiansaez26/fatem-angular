import { TestBed } from '@angular/core/testing';

import { ServiceSelectService } from './service-select.service';

describe('ServiceSelectService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ServiceSelectService = TestBed.get(ServiceSelectService);
    expect(service).toBeTruthy();
  });
});
