import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { NgxSpinnerService } from 'ngx-spinner';
import { TokenService } from '../../../interceptor/token.service';



let dat;
let perfil;
let jwttoken;
const APILoginEndpoint = environment.APILoginEndpoint;
@Injectable({
  providedIn: 'root'
})
export class LoginService {
  err;
  da;
  usuario;
  url = APILoginEndpoint; // endpoint de alarmas

  private isUserLoggedIn;
  public usserLogged: string;
    localStorage: any;

  setUserLoggedIn(user: string) {
    this.isUserLoggedIn = true;
    this.usserLogged = user;
    localStorage.setItem('currentUser', JSON.stringify(user));
    localStorage.setItem('login', 'true');
  }

  setPerfil(Perfil: string) {
    this.isUserLoggedIn = true;
    this.usserLogged = Perfil;
    localStorage.setItem('currentPerfil', JSON.stringify(Perfil));
  }
  getPerfil() {
  return JSON.parse(localStorage.getItem('currentPerfil'));
  }
  public getUserLoggedIn() {
  return JSON.parse(localStorage.getItem('currentUser'));
  }

  getToken() {
  return JSON.parse(localStorage.getItem('AuthToken'));
  }
  constructor(private http: HttpClient, private router: Router,
              private spinner: NgxSpinnerService,
              private tokenService: TokenService) {
  this.isUserLoggedIn = false;
  }
  login(form) {
    const httpOptions = {
    headers: new HttpHeaders({
    'AUTH_API_KEY': 'fatem2019'
    })
    };
    this.spinner.show();
    this.http.post(this.url, form, httpOptions)// llamada a servicio de alarmas
    .subscribe(
      data => {
        dat = data['user']; // obtenemos json de usuario (resultado)
        perfil = data['perfil']; // obtenemos json de usuario (resultado)
        jwttoken = data['jwttoken'];
        this.isUserLoggedIn = true;
        this.usserLogged = dat;
        const u: string = dat;
        const p: string = perfil;
        this.setUserLoggedIn(u);
        this.setPerfil(p);
        // this.usuario=this.getUserLoggedIn();
        this.spinner.hide();
        localStorage.setItem('AuthToken', jwttoken);
        localStorage.setItem('user', dat);
        this.tokenService.setToken(jwttoken);
        this.tokenService.setUserName(dat);
        localStorage.setItem('isLoggedin', 'true');
        this.router.navigate(['/bienvenida']);
        // luego de dar click al boton nos dirigimos a la pantalla "/dashboard"
        /** spinner ends after 5 seconds */
        },
      (error: Response) => {
          this.err = error.status;
          this.spinner.hide();
      }
    );
}
}
