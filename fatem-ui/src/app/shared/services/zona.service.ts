import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { TokenService } from 'src/interceptor/token.service';
import { HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';



const APIMarkEndpoint = environment.APIGetZona;
const APIReportAlarm = environment.APIGetZonaAlarm;

@Injectable({
  providedIn: 'root'
})

export class ZonaService {

  err;
  da;
  usuario;
  datos;
  zonaControl;
  zonas;
  getZona(): Observable<any> {

    const httpOptions = {
      headers: new HttpHeaders({
        'AUTH_API_KEY': 'fatem2019',
      })
    };
    const URL = APIMarkEndpoint;

    return Object(this.http.get(URL, httpOptions));
  }

  getZonaAlarm(): Observable<any> {

    const httpOptions = {
      headers: new HttpHeaders({
        'AUTH_API_KEY': 'fatem2019',
      })
    };
    const URL = APIReportAlarm;

    return Object(this.http.get(URL, httpOptions));
  }

  constructor(private http: HttpClient,
    private tokenService: TokenService) {
  }
}
