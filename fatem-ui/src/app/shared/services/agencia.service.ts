import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { TokenService } from 'src/interceptor/token.service';
import { HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

const APIMarkEndpoint = environment.APIGetAgencia;
const APIAgenciaAlarm = environment.APIGetAgenciaAlarm;


@Injectable({
  providedIn: 'root'
})
export class AgenciaService {

  err;
  da;
  usuario;
  datos;
  zonaControl;
  zonas;
  getAgencia(form) : Observable <any>{

    const httpOptions = {
      headers: new HttpHeaders({
      'AUTH_API_KEY': 'fatem2019',
      })
      };
      const URL = APIMarkEndpoint;

      return Object(this.http.get(URL+'/'+form, httpOptions));
    }

    getAgenciaAlarm(form) : Observable <any>{

      const httpOptions = {
        headers: new HttpHeaders({
        'AUTH_API_KEY': 'fatem2019',
        })
        };
        const URL = APIAgenciaAlarm;
  
        return Object(this.http.get(URL+'/'+form, httpOptions));
      }


  constructor(private http: HttpClient,
    private tokenService: TokenService) {
    }
}

