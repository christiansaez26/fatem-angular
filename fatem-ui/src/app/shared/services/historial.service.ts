import { Injectable, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatPaginatorIntl } from '@angular/material';
import {environment} from '../../../environments/environment';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { PeriodicElement } from 'src/app/layout/dashboard1/dashboard1.component';
import { NgxSpinnerService } from 'ngx-spinner';
import { TranslateService } from '@ngx-translate/core';

let dat;
let dataTabla: MatTableDataSource<PeriodicElement>;
// variable con estructura de "PeriodicElement"
const APIHistoryEndpoint = environment.APIHistoryEndpoint;
// endpoint historial
@Injectable({ // metodo para ser llamado desde cualquier componente hermano
providedIn: 'root'
})
export class HistorialService {
err;
dataTable;
column;
planta;
url = APIHistoryEndpoint;
@ViewChild(MatPaginator) paginator: MatPaginator;
// declaracion de paginador
@ViewChild(MatSort) sort: MatSort;
public activeLang = 'es';
public pageSize = 10;
// capacidad de registros en la tabla
public currentPage = 0;
public totalSize;
public displayedColumns: string[];
  filtro;
  disable: boolean;
  fecValid: any;
  filtro2: any;
public handlePage(e: any) {
// metodo para ejecutarse cuando esta disponible la variacion de registros en tabla
this.currentPage = e.pageIndex;
this.pageSize = e.pageSize;
this.iterator();
}
constructor(private http: HttpClient, private spinner: NgxSpinnerService, private translate: TranslateService) {
this.translate.setDefaultLang(this.activeLang);
}
cabecera(columnas) {
this.currentPage = null;
this.totalSize = null;
this.column = columnas;
columnas = null;
}

limpiaTabla(){
  this.currentPage = null;
  this.totalSize = null;
  this.dataTable = null;
}
consultar(form, obligatorio) {
this.err = null;
dat = null;
this.dataTable = null;
this.filtro=obligatorio;
const httpOptions = {
headers: new HttpHeaders({
'AUTH_API_KEY': 'fatem2019'
})
};
this.spinner.show();
// this.paginator._intl.itemsPerPageLabel="Elementos por persona";
this.http.post(this.url, form, httpOptions)
// llamada del servicio historial
.subscribe(
  data => {
    // muestra en consola respuesta del servicio historial
    dat = data['listHistoricoAlarma'];
    // json de "listHistoricoAlarma;
    dataTabla = new MatTableDataSource<PeriodicElement>(dat);
    // la respuesta la almacena en variable con el modelo "PeriodicElement"
    dataTabla.paginator = this.paginator;
    // datatabla se le agrega paginador segun la cantidad de datos
    this.totalSize = dataTabla.data.length;
    // se agrega la cantidad maxima de registros
    this.iterator();
    this.disable=true;
    // se itera el metodo para dividir la data en la tabla
    this.spinner.hide();
  },
  error => {
  this.err = error.status; // captura error
  this.currentPage = null;
  this.totalSize = null;
  this.disable=false;
  this.spinner.hide();
  }
  );
  }

private iterator() {
const end = (this.currentPage + 1) * this.pageSize;
const start = this.currentPage * this.pageSize;
const part = dataTabla.data.slice(start, end);
this.dataTable = part;
return `${start + 1} - ${end} de ${this.totalSize}`;
}
}
export function  getDutchPaginatorIntl() {
  const paginatorIntl = new MatPaginatorIntl();
  paginatorIntl.itemsPerPageLabel = 'Elemento por pagina:';
  return paginatorIntl;
}
