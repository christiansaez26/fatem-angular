import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { TokenService } from 'src/interceptor/token.service';


const APIMarkSelectInfoEndpoint = environment.APIGetSelect;

@Injectable({
  providedIn: 'root'
})


export class ServiceSelectService {

  err;
  da;
  usuario;
  datos;
  getSelect(): Observable<any> {

    const url = APIMarkSelectInfoEndpoint; //

    const httpOptions = {
      headers: new HttpHeaders({
        'AUTH_API_KEY': 'fatem2019',
      })
    };

    return Object(this.http.get(url, httpOptions));
   
  }


  constructor(private http: HttpClient,
    private tokenService: TokenService) {
  }
}
