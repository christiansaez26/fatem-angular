import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { TokenService } from 'src/interceptor/token.service';
import { HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';



const APIMarkFileInfoEndpoint = environment.APIAddFile;
const APIMarkGuardarInfoEndpoint = environment.APISave;

@Injectable({
  providedIn: 'root'
})

export class ArchivoService {

  err;
  da;
  usuario;
  datos;
  GuardaRegistro(form): Observable<any> {

    const url = APIMarkGuardarInfoEndpoint; //

    const httpOptions = {
      headers: new HttpHeaders({
        'AUTH_API_KEY': 'fatem2019',
      })
    };

    let fileInput: any = document.getElementById("img");
    let files = fileInput.files[0];

    var formData = new FormData();
    formData.append("file", files);

    formData.append("ad",JSON.stringify(form))  

    return Object(this.http.post(url,formData, httpOptions));


  }


  constructor(private http: HttpClient,
    private tokenService: TokenService) {
  }

  addFile(file): Observable<any> {

    const url = APIMarkFileInfoEndpoint;

    const httpOptions = {
      headers: new HttpHeaders({
        'AUTH_API_KEY': 'fatem2019',
      })
    };

    return Object(this.http.post(url,file, httpOptions));

  }
}
