import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ActivityMonitorService {
  estado: any;
  val: string;

  constructor() { }

  setStatus(status: any) {
    this.estado = status;
    this.val = 'Perfil';
}
}
