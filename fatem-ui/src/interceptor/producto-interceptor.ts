import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TokenService } from '../interceptor/token.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ProductoInterceptorService implements HttpInterceptor {
  user;
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
  let autReq = req;
  this.user = localStorage.getItem('user');
  const token = this.tokenService.getToken();

  if(token==null){
    localStorage.setItem('login', 'true');
    localStorage.removeItem('isLoggedin');
    this.router.navigate(['/login']);
  }
  if (this.user === null) {
    autReq = req.clone({ headers: req.headers.set('Authorization', '') });
  } else if (token !== null) {
    autReq = req.clone({ headers: req.headers.set('Authorization', 'FatemToken' + token) });
  }
  return next.handle(autReq);
}
  constructor(private tokenService: TokenService,private router: Router) { }
}
export const interceptorProvider = [{provide: HTTP_INTERCEPTORS, useClass: ProductoInterceptorService, multi: true}];