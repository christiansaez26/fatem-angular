
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

/*export const environment = {
  production: false,
  APILoginEndpoint: 'http://fatem-api-login-tch-fatem.infra.preprod.ocp.tchile.local/fatem/login/api/login',
  APIGetAlarmsEndpoint: 'http://fatem-api-alarm-tch-fatem.infra.preprod.ocp.tchile.local/fatem/alarma/api/getAlarms',
  APIHistoryEndpoint: 'http://fatem-api-alarm-tch-fatem.infra.preprod.ocp.tchile.local/fatem/alarma/api/getHistoryAlarms',
  APITicketEndpoint: 'http://fatem-api-ticket-tch-fatem.infra.preprod.ocp.tchile.local/fatem/ticket/api/getTicket',
  APIGetArchivo: '',
  APIGetSelect:''

};*/

 export const environment = {
  production: false,
  APILoginEndpoint: 'http://localhost:8083/fatem/login/api/login',
  APIGetAlarmsEndpoint: 'http://localhost:8084/fatem/alarm/api/getAlarms',
  APIHistoryEndpoint:'http://localhost:8084/fatem/history/api/getHistoryAlarms',
  APITicketEndpoint:'http://localhost:8085/fatem/ticket/api/getTicket',
  APISave: 'http://localhost:8088/fatem/markinfo/api/save',
  APIGetSelect:'http://localhost:8088/fatem/markinfo/api/getMarkInfo',
  APIGetZona: 'http://localhost:8088/fatem/markinfo/api/getZona',
  APIGetAgencia: 'http://localhost:8088/fatem/markinfo/api/getAgency',
  APIAddFile:'http://localhost:8088/fatem/markinfo/api/addFile',
  APIGetZonaAlarm:'http://localhost:8084/fatem/alarm/api/getZona',
  APIGetAgenciaAlarm:'http://localhost:8084/fatem/alarm/api/getAgency'
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
