export const environment = {
    production: false,
    APILoginEndpoint: 'http://fatem-api-login-tch-fatem.infra.preprod.ocp.tchile.local/fatem/login/api/login',
	
    APIGetAlarmsEndpoint: 'http://fatem-api-alarm-tch-fatem.infra.preprod.ocp.tchile.local/fatem/alarma/api/getAlarms',
	APIGetZonaAlarm: 'http://fatem-api-alarm-tch-fatem.infra.preprod.ocp.tchile.local/fatem/alarma/api/getZona',
    APIGetAgenciaAlarm: 'http://fatem-api-alarm-tch-fatem.infra.preprod.ocp.tchile.local/fatem/alarma/api/getAgency',
	
    APIHistoryEndpoint: 'http://fatem-api-alarm-tch-fatem.infra.preprod.ocp.tchile.local/fatem/alarma/api/getHistoryAlarms',
	
    APITicketEndpoint: 'http://fatem-api-ticket-tch-fatem.infra.preprod.ocp.tchile.local/fatem/ticket/api/getTicket',
	
	APISave: 'http://fatem-api-markinfo-tch-fatem.infra.preprod.ocp.tchile.local/fatem/markinfo/api/save',
    APIGetSelect: 'http://fatem-api-markinfo-tch-fatem.infra.preprod.ocp.tchile.local/fatem/markinfo/api/getMarkInfo',
    APIGetZona: 'http://fatem-api-markinfo-tch-fatem.infra.preprod.ocp.tchile.local/fatem/markinfo/api/getZona',
    APIGetAgencia: 'http://fatem-api-markinfo-tch-fatem.infra.preprod.ocp.tchile.local/fatem/markinfo/api/getAgency',
    APIAddFile: 'http://fatem-api-markinfo-tch-fatem.infra.preprod.ocp.tchile.local/fatem/markinfo/api/addFile',
	
    env: 'cert'
  };